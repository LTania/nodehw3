const Joi = require('joi');

module.exports.validateAuth = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .required(),
    password: Joi.string()
        .required(),
    role: Joi.string()
        .required(),
  });
  await schema.validateAsync(req.body);
  next();
};
