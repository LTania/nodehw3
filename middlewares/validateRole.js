const {User} = require('../models/userModel');

module.exports.isDriver = async (req, res, next) => {
  const user = await User.findOne({_id: req.user._id});
  if (user.role === 'DRIVER') {
    next();
  } else {
    res.status(400).json({
      message: 'Forbidden',
    });
  }
};

module.exports.isShipper = async (req, res, next) => {
  const user = await User.findOne({_id: req.user._id});
  if (user.role === 'SHIPPER') {
    next();
  } else {
    res.status(400).json({
      message: 'Forbidden',
    });
  }
};
