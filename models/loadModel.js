const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  created_by: {
    required: true,
    type: String,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  status: {
    required: true,
    type: String,
    default: 'NEW',
    enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
  },
  state: {
    type: String,
    enum: ['En route to Pick Up', 'Arrived to Pick Up',
      'En route to delivery', 'Arrived to delivery'],
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  payload: {
    type: Number,
    required: true,
  },
  logs: [{
    message: {
      type: String,
    },
    time: {
      type: String,
      default: new Date(),
    },
  }],
});

module.exports.Load = mongoose.model('Load', loadSchema);
