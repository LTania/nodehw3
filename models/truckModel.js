const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    required: true,
    type: String,
  },
  type: {
    type: String,
    required: true,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
  assigned_to: {
    type: String,
    default: '',
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
