const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const PORT = process.env.PORT || 8080;
const aRouter = require('./routers/authRouter');
const uRouter = require('./routers/userRouter');
const tRouter = require('./routers/truckRouter');
const lRouter = require('./routers/loadRouter');

const app = express();
app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/auth', aRouter);
app.use('/api/users/me', uRouter);
app.use('/api/trucks', tRouter);
app.use('/api/loads', lRouter);

const start = async () => {
  await mongoose.connect(
      'mongodb+srv://nodehw:nodehw@cluster0.zgh29.mongodb.net/myfavtask3?retryWrites=true&w=majority',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );

  app.listen(PORT, () =>{
    console.log(`Server has been started on ${PORT}...`);
  });
};

start();


