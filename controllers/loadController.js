const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

module.exports.createLoad = async (req, res) => {
  try {
    // eslint-disable-next-line camelcase
    const {name, payload, pickup_address, delivery_address,
      dimensions} = req.body;
    const {width, length, height} = dimensions;
    const userId = req.user._id;

    const load = new Load({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions: {
        width,
        length,
        height,
      },
      logs: [{message: `Load created by ${req.user.email}`,
        time: new Date()},
      ],
      created_by: userId,
    });

    await load.save();
    res.json({message: 'Load created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getLoads = async (req, res) => {
  try {
    let {status, limit, offset} = req.query;
    if (offset) {
      offset = Number(offset);
    } else {
      offset = 0;
    };
    if (limit) {
      limit = Number(limit);
    } else {
      limit = 10;
    };
    const user = await User.findOne({_id: req.user._id});
    if (user.role == 'SHIPPER') {
      Load.find({created_by: req.user._id, status: status || 'NEW'},
          null, {skip: offset, limit: limit})
          .exec()
          .then((loads) => {
            res.json({loads});
          })
          .catch((err) => {
            res.status(500).json({message: err.message});
          });
    }
    if (user.role == 'DRIVER') {
      Load.find({assigned_to: req.user._id, status: status || 'NEW'},
          null, {skip: offset, limit: limit})
          .exec()
          .then((loads) => {
            res.json({loads});
          })
          .catch((err) => {
            res.status(500).json({message: err.message});
          });
    }
  } catch (err) {
    res.status(400).json({message: err.message});
  }
};

module.exports.patchLoad = async (req, res) => {
  const loadsStates = ['En route to Pick Up',
    'Arrived to Pick Up', 'En route to delivery', 'Arrived to'];
  const userId = req.user._id;

  try {
    const currentLoad = await Load.findOne({
      assigned_to: userId,
      status: 'ASSIGNED',
    });
    const currentLoadState = currentLoad.state || '';
    const index = loadsStates.indexOf(currentLoadState) || 0;
    if (index === loadsStates.length) {
      res.json({
        message: 'Already delivered',
      });
    }
    await Load.findOneAndUpdate({_id: currentLoad.id}, {
      state: loadsStates[index + 1],
    }).exec();

    res.json({
      message: `Load state changed to ${loadsStates[index + 1]}`,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getActiveLoads = async (req, res) => {
  const userId = req.user._id;

  const load = await Load.findOne({
    assigned_to: userId,
    status: 'ASSIGNED',
  }).exec();

  if (!load) {
    return res.status(400).json({message: 'No active loads'});
  }

  res.status(200).json({load});
};

module.exports.getLoadById = async (req, res) => {
  const loadId = req.params.id;
  const userId = req.user._id;
  try {
    const load = await Load.findOne({_id: loadId, created_by: userId}).exec();

    if (!load) res.status(400).json({message: 'No load found'});

    res.json({load});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.updateLoad = async (req, res) => {
  const loadId = req.params.id;
  const userId = req.user._id;
  const fieldsToUpdate = req.body;

  const load = await Load.findOne({_id: loadId, created_by: userId});

  if (!load) {
    return res.status(400).json({message: 'No load with such id'});
  }

  await load.updateOne({...fieldsToUpdate});

  load.save();

  res.status(200).json({message: 'Load details changed successfully'});
};

module.exports.deleteLoad = async (req, res) => {
  const {id} = req.params;

  try {
    await Load.deleteOne({_id: id});
  } catch (err) {
    return res.status(400).json({message: 'Something went wrong'});
  }

  res.status(200).json({message: 'Load deleted successfully'});
};

module.exports.postLoad = async (req, res) => {
  const loadId = req.params.id;
  const userId = req.user._id;

  if (!loadId) res.status(400).json({message: 'No load found'});

  try {
    console.log(userId);
    console.log(loadId);
    const currentLoad = await Load.findOneAndUpdate({
      _id: loadId,
      created_by: userId,
      status: 'NEW',
    }, {
      status: 'POSTED',
    }).exec();
    console.log(currentLoad);

    const {payload, dimensions} = currentLoad;
    const {width, height, length} = dimensions;

    const type = findTypeTruck(payload, width, height, length, res);

    const suitableTruck = await Truck.findOne({
      status: 'IS',
      type,
    }).exec();

    if (!suitableTruck) {
      await Load.findOneAndUpdate({
        _id: loadId,
        created_by: userId,
        status: 'POSTED',
      }, {
        status: 'NEW',
      }).exec();

      res.status(200).json({
        message: 'No truck available',
        driver_found: false,
      });
    } else {
      await Load.findOneAndUpdate({
        _id: loadId,
        created_by: userId,
        status: 'POSTED',
      }, {
        status: 'ASSIGNED',
        state: 'En route to Pick Up',
        assigned_to: suitableTruck.assigned_to,
      }).exec();

      res.status(200).json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getInfo = async (req, res) => {
  const loadId = req.params.id;
  const userId = req.user._id;

  try {
    const load = await Load.findOne({_id: loadId, created_by: userId}).exec();
    const truck = await Truck.findOne({assigned_to: load.assigned_to}).exec();

    if (load && truck) {
      res.json({
        load,
        truck,
      });
    } else {
      res.status(400).json({message: 'string'});
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

const findTypeTruck = (payload, width, height, length, res) => {
  if (width >= 700 || length >= 350 || height >= 200 || payload > 4000) {
    res.status(400).json({message: 'Too big'});
    return;
  }
  if (width >= 500 || length >= 250 || height >= 170 || payload >= 2500) {
    return 'LARGE STRAIGHT';
  }
  if (width >= 300 || length >= 250 || height >= 170 || payload >= 1700) {
    return 'SMALL STRAIGHT';
  }
  return 'SPRINTER';
};


