const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

module.exports.register = async (req, res) => {
  const {email, password, role} = req.body;
  if (email && password && role) {
    const user = await User.findOne({email});
    if (user) {
      res.status(400).json({message: 'User already exist'});
    } else {
      const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role,
      });
      await user.save();

      res.status(200).json({message: 'Profile created successfully'});
    }
  } else {
    res.status(400).json({message: 'Enter credentials'});
  }
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  if (!email && !password) {
    res.status(400).json({message: 'Enter credentials!'});
  } else {
    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json({message: `No user ${email}`});
    }

    if (!(await bcrypt.compare(password, user.password))) {
      return res.status(400).json({message: `Wrong password`});
    }

    const token = jwt.sign({
      _id: user._id,
      email: user.email,
    }, JWT_SECRET);
    res.status(200).json({jwt_token: token});
  }
};

module.exports.forgotPassword = async (req, res) => {
  const {email} = req.body;

  User.findOne({email}).exec()
      .then((user) => {
        if (!user) {
          return res.status(400).json({message: 'No user'});
        }
        return res.json({message: 'New password sent to your email address'});
      })
      .catch(() => {
        return res.status(500).json({message: 'Cannot reset pass'});
      });
};

