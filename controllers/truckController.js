const {Truck} = require('../models/truckModel');

module.exports.getTrucks = async (req, res) => {
  try {
    // eslint-disable-next-line camelcase
    const created_by = req.user._id;

    const trucks = await Truck.find({
      created_by,
    }).exec();

    if (!trucks) res.status(400).json({message: 'No trucks'});
    res.json({trucks});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.createTruck = async (req, res) => {
  try {
    const {type} = req.body;
    if (!type) res.status(400).json({message: 'Enter correct data'});

    const truck = new Truck({
      created_by:
      req.user._id,
      type,
      created_date: new Date(),
    });
    await truck.save();

    res.json({message: 'Truck created successfully'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.getTruck = async (req, res) => {
  try {
    const truckId = req.params.id;
    const userId = req.user._id;

    if (!truckId) res.status(400).json({message: 'Please enter correct id'});

    const truckInfo = await Truck.findOne(
        {_id: truckId, created_by: userId}).exec();
    res.json(truckInfo);
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.updateTruckType = async (req, res) => {
  const truckId = req.params.id;
  const userId = req.user._id;

  if (!truckId) res.status(400).json({message: 'No truck found'});

  try {
    const userTruck = await Truck.findOne(
        {_id: truckId, created_by: userId}).exec();

    if (!userTruck) res.status(400).json({message: 'Truck not found'});

    if (userTruck.assigned_to) {
      res.status(400).json({message: 'You cannot change the assigned truck'});
    } else {
      await Truck.findOneAndUpdate({
        _id: truckId,
        created_by: userId,
      }, {
        type: req.body.type,
      }).exec();

      res.json({message: 'Truck details changed successfully'});
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.deleteTruck = async (req, res) => {
  try {
    const truckId = req.params.id;
    const userId = req.user._id;
    const userTruck = await Truck.findOne(
        {_id: truckId, created_by: userId}).exec();

    if (!userTruck) res.status(400).json({message: 'Truck not found'});

    if (userTruck.assigned_to) {
      res.status(400).json({message: 'You cannot delete the assigned truck'});
    } else {
      await Truck.findOneAndDelete({
        _id: truckId,
        created_by: userId,
      }).exec();
      res.status(200).json({message: 'Truck deleted successfully'});
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

module.exports.assignTruck = async (req, res) => {
  try {
    const truckId = req.params.id;
    const userId = req.user._id;
    const isAssigned = await Truck.findOne({assigned_to: userId}).exec();

    if (isAssigned) {
      res.status(400).json({message: 'You have truck!'});
    } else {
      await Truck.findOneAndUpdate({_id: truckId}, {
        assigned_to: userId,
        status: 'IS',
      }).exec();
      res.status(200).json({message: 'Truck assigned successfully'});
    }
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

