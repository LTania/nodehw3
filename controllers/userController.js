const {User} = require('../models/userModel');

module.exports.deleteMe = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  if (!user) {
    return res.status(400).json({message: 'User doesnt exist'});
  }
  await User.deleteOne({_id: req.user._id});
  return res.status(200).json({message: 'Success'});
};

module.exports.getProfile = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  res.status(200).json({
    _id: req.user._id,
    email: user.email,
    role: user.role,
    created_date: user.created_date,
  });
};

module.exports.changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;

  if (oldPassword === newPassword) {
    return res.status(400).json({
      message: 'New password should differ from old one',
    });
  }

  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }

  const verified = await bcrypt.compare(oldPassword, user.password);

  if (!verified) {
    return res.status(400).json({message: 'Wrong current password'});
  }

  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});

  user.save();

  res.status(200).json({message: 'Success'});
};
