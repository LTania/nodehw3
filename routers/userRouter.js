const express = require('express');
const router = new express.Router();
const {deleteMe,
  getProfile,
  changePassword} = require('../controllers/userController');
const {asyncWrapper} = require('../helpers/asyncWrapper');
const {validateToken} = require('../middlewares/validateToken');


router.delete('/', validateToken, asyncWrapper(deleteMe));
router.get('/', validateToken, asyncWrapper(getProfile));
router.patch('/password', validateToken, asyncWrapper(changePassword),
);


module.exports = router;
