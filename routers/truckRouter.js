const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/asyncWrapper');
const {validateToken} = require('../middlewares/validateToken');
const {getTrucks,
  createTruck,
  getTruck,
  updateTruckType,
  deleteTruck,
  assignTruck} = require('../controllers/truckController');
const {isDriver} = require('../middlewares/validateRole');

router.get('/', validateToken, isDriver, asyncWrapper(getTrucks));
router.post('/', validateToken, isDriver, asyncWrapper(createTruck));
router.get('/:id', validateToken, isDriver, asyncWrapper(getTruck));
router.put('/:id', validateToken, isDriver, asyncWrapper(updateTruckType));
router.delete('/:id', validateToken, isDriver, asyncWrapper(deleteTruck));
router.post('/:id/assign', validateToken, isDriver, asyncWrapper(assignTruck));

module.exports = router;
