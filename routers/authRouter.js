const express = require('express');
const router = new express.Router();
const {register,
  login,
  forgotPassword} = require('../controllers/authController');
const {asyncWrapper} = require('../helpers/asyncWrapper');
const {validateAuth} = require('../middlewares/validateAuth');

router.post('/register', asyncWrapper(validateAuth), asyncWrapper(register));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = router;
