const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('../helpers/asyncWrapper');
const {validateToken} = require('../middlewares/validateToken');
const {isDriver, isShipper} = require('../middlewares/validateRole');
const {createLoad,
  getLoads,
  patchLoad,
  getActiveLoads,
  getLoadById,
  updateLoad,
  deleteLoad,
  postLoad,
  getInfo} = require('../controllers/loadController');

router.get('/', validateToken, asyncWrapper(getLoads));
router.post('/', validateToken, isShipper, asyncWrapper(createLoad));
router.get('/active', validateToken, isDriver, asyncWrapper(getActiveLoads));
router.patch('/active/state', validateToken, isDriver, asyncWrapper(patchLoad));
router.get('/:id', validateToken, asyncWrapper(getLoadById));
router.put('/:id', validateToken, isShipper, asyncWrapper(updateLoad));
router.delete('/:id', validateToken, isShipper, asyncWrapper(deleteLoad));
router.post('/:id/post', validateToken, isShipper, asyncWrapper(postLoad));
router.get('/:id/shipping_info',
    validateToken, isShipper, asyncWrapper(getInfo));

module.exports = router;
